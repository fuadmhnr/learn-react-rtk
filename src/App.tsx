import './App.css'
import UserList from './components/User'


const App = () => {
  return (
    <div>
      <h2> Redux complete tutorial </h2>
      <UserList />
    </div>
  )
}

export default App
