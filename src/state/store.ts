import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './counter/counterSlice'
import userReducer, { fetchUsers } from './user/userSlice'
import createSagaMiddleware from 'redux-saga'
import { helloSaga } from './sagas'

const sagaMiddleware = createSagaMiddleware()

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    user: userReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(sagaMiddleware)
})

sagaMiddleware.run(helloSaga);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch & {
  fetchUsers: typeof fetchUsers;
}
