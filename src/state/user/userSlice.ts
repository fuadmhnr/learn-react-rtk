import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}


interface UserState {
  users: User[]
  status: 'idle' | 'loading' | 'success' | 'failed'
  error: string | null
}

const initialState: UserState = {
  users: [],
  status: 'idle',
  error: null
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(fetchUsers.pending, (state) => {
      state.status = 'loading'
    }).addCase(fetchUsers.fulfilled, (state, action) => {
      state.status = 'success',
        state.users = action.payload
    }).addCase(fetchUsers.rejected, (state, action) => {
      state.status = 'failed',
        state.error = action.error.message || 'An error occurred'
    })
  },
})

export const fetchUsers = createAsyncThunk(
  "users/fetchUsers",
  async () => {
    const response = await axios.get('https://jsonplaceholder.typicode.com/users')
    return response.data as User[]
  }
)

export default userSlice.reducer;
