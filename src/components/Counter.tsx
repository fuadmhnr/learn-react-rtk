import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../state/store";
import { decrement, decrementByAmount, increment, incrementByAmount, incrementAsync } from "../state/counter/counterSlice";

const Counter = () => {
  const count = useSelector((state: RootState) => state.counter.value)
  const dispatch = useDispatch<AppDispatch>();

  return (
    <div>
      <h1>{count}</h1>
      <div>
        <button onClick={() => dispatch(incrementAsync(50))}>Async Increment by 50</button>
        <button onClick={() => dispatch(incrementByAmount(25))}>Increment by 25</button>
        <button onClick={() => dispatch(increment())}>Increment</button>
        <button onClick={() => dispatch(decrement())}>Decrement</button>
        <button onClick={() => dispatch(decrementByAmount(25))}>Decrement by 25</button>
      </div>
    </div>
  )
}

export default Counter;
